var LocalStrategy   = require('passport-local').Strategy;
var User = require('../routes/model_db').User;
var bCrypt = require('bcrypt-nodejs');

exports.registerUsers = function(req, res) {
	findOrCreateUser = function(){
		// find a user in Mongo with provided username
    User.findOne({ 'username' :  req.body.username }, function(err, user) {
    	// In case of any error, return using the done method
      if (err){
      	console.log('Error in SignUp: '+err);
        return done(err);
      }
      // already exists
      if (user) {
      	console.log('User already exists with username: '+ req.body.username);
				res.render('registro', {message: 'Usuario ya existe'})
      } else {
      	// if there is no user with that email
        // create the user
        var newUser = new User();

        // set the user's local credentials
        newUser.username = req.body.username;
        newUser.password = createHash(req.body.password);
				newUser.roll     = req.body.roll || 'user';

        // save the user
      	newUser.save(function(err) {
          if (err){
          	console.log('Error in Saving user: '+err);
            throw err;
          }
          console.log('User Registration succesful');
          res.render('registro', {message: 'Usuario registrado correctamente. Ya puedes iniciar sesión'})
        });
			}
    });
  };
  // Delay the execution of findOrCreateUser and execute the method
  // in the next tick of the event loop
  process.nextTick(findOrCreateUser);

	var createHash = function(password){
		return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
  }
}

exports.registerAdmins = function(req, res) {
	findOrCreateUser = function(){
		// find a user in Mongo with provided username
    User.findOne({ 'username' :  req.body.username }, function(err, user) {
    	// In case of any error, return using the done method
      if (err){
      	console.log('Error in SignUp: '+err);
        return done(err);
      }
      // already exists
      if (user) {
      	console.log('User already exists with username: '+ req.body.username);
				res.render('registerAdmin', {message: 'Usuario ya existe'})
      } else {
      	// if there is no user with that email
        // create the user
        var newUser = new User();

        // set the user's local credentials
        newUser.username = req.body.username;
        newUser.password = createHash(req.body.password);
				newUser.roll     = req.body.roll || 'user';

        // save the user
      	newUser.save(function(err) {
          if (err){
          	console.log('Error in Saving user: '+err);
            throw err;
          }
          console.log('User Registration succesful');
          res.render('registerAdmin', {message: 'Usuario registrado correctamente. Ya puedes iniciar sesión'})
        });
			}
    });
  };
  // Delay the execution of findOrCreateUser and execute the method
  // in the next tick of the event loop
  process.nextTick(findOrCreateUser);

	var createHash = function(password){
		return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
  }
}

exports.editPassword = function(req, res) {
	findOrCreateUser = function(){
		// find a user in Mongo with provided username
    User.findOne({ 'username' :  req.body.username }, function(err, user) {
    	// In case of any error, return using the done method
      if (err){
      	console.log('Error in Update: '+err);
        res.render('cambiar', {message1: 'Usuario no existe'})
      } else {
				if (user.roll != 'admin') {
					user.password = createHash(req.body.password);
	      	user.save(function(err) {
	          if (err){
	          	console.log('Error in Saving user: '+err);
	            throw err;
	          }
	          console.log('User Registration succesful');
	          res.render('cambiar', {message1: 'Contraseña actualizada correctamente'})
	        });
				} else {
					res.render('cambiar', {message1: 'No tiene permisos para actualizar la contraseña a este usuario'})
				}
			}
    });
  };

	process.nextTick(findOrCreateUser);

	var createHash = function(password){
		return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
	}
}
exports.editPasswordAdmin = function(req, res) {
	findOrCreateUser = function(){
		// find a user in Mongo with provided username
    User.findOne({ 'username' :  req.body.username }, function(err, user) {
    	// In case of any error, return using the done method
      if (err){
      	console.log('Error in Update: '+err);
        res.render('cambiarAdmin', {message1: 'Usuario no existe'})
      } else {
				if (user.roll = 'admin') {
					user.password = createHash(req.body.password);
	      	user.save(function(err) {
	          if (err){
	          	console.log('Error in Saving user: '+err);
	            throw err;
	          }
	          console.log('User Registration succesful');
	          res.render('cambiarAdmin', {message1: 'Contraseña actualizada correctamente'})
	        });
				} else {
					res.render('cambiarAdmin', {message1: 'No tiene permisos para actualizar la contraseña a este usuario'})
				}
			}
    });
  };

  // Delay the execution of findOrCreateUser and execute the method
  // in the next tick of the event loop
  process.nextTick(findOrCreateUser);

	var createHash = function(password){
		return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
  }
}
