var mongoose = require( 'mongoose' );
//To use the model I created a variable regModel
var regModel = require('./model_db').Tesis;


exports.index = function(req, res){
  res.render('index', {
    title: 'Ejemplo de Passport JS',
    user: req.user
  });
};

exports.getAll = function (req, res){
  regModel.find(function (err, entries) {
    if (!err) {
      res.render( 'principal', {
        entries : entries
      });
    } else {
      console.log(err);
    }
  });
};

exports.getJefe = function (req, res){
  if(req.user.roll === 'admin') {
    regModel.find(function (err, entries) {
      if (!err) {
        res.render( 'principalJefe', {
          entries : entries
        });
      } else {
        console.log(err);
      }
    });
  } else {
    res.redirect('/jefeDP');
  }
}

exports.getGradu = function (req, res){
  if(req.user.roll === 'user') {
    regModel.find(function (err, entries) {
      if (!err) {
        res.render( 'principalGradu', {
          entries : entries
        });
      } else {
        console.log(err);
      }
    });
  } else {
    res.redirect('/graduandos');
  }
}

exports.getRegis = function (req, res){
  if(req.user.roll === 'office') {
    res.render('registro');
  } else {
    res.redirect('/secretaria');
  }
}

exports.getAutorb = function (req, res){
  regModel.find(function (err, entries) {
    if (!err) {
      res.render( 'autorBuscar', {
        entries : entries
      });
    } else {
      console.log(err);
    }
  });
}

exports.postnew = function (req, res){
  var entry;
  //console.log("POST: " + req.params + req.body + req.query);
  entry = new regModel();
  req.body.claves = req.body.claves.split(" ");
  req.body.userAdd = req.user._id;

  for (key in req.body){
    entry[key] = req.body[key];
  }

  entry.save(function (err) {
    if (!err) {
      console.log("created");
      res.redirect('/agregarProyecto');
    } else {
      console.log(err);
      res.redirect('/');
    }
  });
};

exports.getById = function (req, res){
  regModel.findById(req.params.id, function (err, entry) {
    if (!err) {
      res.send(entry);
    } else {
      console.log(err);
    }
  });
};

exports.getById_complex = function (req, res){
  regModel.findById(req.params.id, function (err, entry) {
    var frase = entry.toString();
    var subarr = frase.split(': ');
    //Final array
    var keys = new Array();
    var values = new Array();

    for (var i=0;i<subarr.length;i++){
      var clave = subarr[i].substring(subarr[i].lastIndexOf(' ') + 1);
      clave = clave.trim();
      if (i < subarr.length - 1){
        var valor  = subarr[i+1].substring(0, subarr[i+1].indexOf(','));
      }

      if (i == subarr.length - 2){
        valor = subarr[subarr.length - 1].substring(0, subarr[i + 1].lastIndexOf('}') - 1);
      }
      valor = valor.trim();
      console.log(i + ' : ' + valor);

      var vsExprReg = /^[a-z\sáéíóúñ.,_\-\&\/]+$/i;
      if (vsExprReg.test(clave)) {
        keys[i] = clave;
        values[i] = valor;
      }
    }

    if (!err) {
      res.render( 'editar', {
          id : entry.id,
          keys : keys,
          values : values
      });
    } else {
      console.log(err);
    }
  });
};

exports.putById = function (req, res){
  //console.log(req.body);
  //req.body.claves = req.body.claves.split(" ");
  regModel.findById(req.params.id, function (err, entry) {
    for (key in req.body){
      entry[key] = req.body[key];
    }
    entry.save(function (err) {
      if (!err) {
        console.log("updated");
        //Es imprescindible devolver datos, en este caso las llaves
        res.status(201).send({});
      } else {
        console.log(err);
        res.status(500).send("updated error");
      }
      // res.send(entry);
    });
  });
};

exports.deleteById = function (req, res){

  regModel.findById(req.params.id, function (err, entry) {
    entry.remove(function (err) {
      if (!err) {
        console.log("removed");
        res.status(201).send({});
      } else {
        console.log(err);
        rres.status(500).send("removed error");
      }
    });
  });
};

exports.searchByName = function (req, res){
  regModel.find({ name: req.params.name }, function (err, entry) {
    res.status(200).send({entries: entry});
  })
}

exports.searchByTitle = function (req, res){
  regModel.findOne({ titulo: req.params.titulo }, function (err, entry) {
    res.status(200).send({entries: entry});
  })
}

exports.getTesisPersonal = function (req, res){
  if(req.user.roll === 'user') {
    regModel.findOne({userAdd: req.user._id}, function (err, entries) {
      if (!err) {
        res.render( 'agregarProyecto', {
          entries : entries
        });
      } else {
        console.log(err);
      }
    });
  } else {
    res.redirect('/graduandos');
  }
}
