var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var Model = new Schema({
    titulo : String,
    name : String,
    movil : Number,
    email : String,
    claves : [],
    sem : String
});

var UserSchema = new Schema({
  username: String,
  password: String,
  roll: String
});

var Ini = new Schema({
	name				: String,
	provider_id : {type: String, unique: true}, // ID que proporciona Twitter o Facebook
  photo			 : String, // Avatar o foto del usuario
	createdAt	 : {type: Date, default: Date.now} // Fecha de creación
});

//To use the model I created a variable regModel:
var models = {
  Tesis: mongoose.model('tesis', Model),
  User: mongoose.model('user', UserSchema),
  Inicio = mongoose.model('inicio', Ini);

}

module.exports = models;

mongoose.connect( 'mongodb://localhost/db6' );
