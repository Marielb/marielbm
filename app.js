require( './routes/model_db' ); //for DB mongoose.
require('./passport')(passport);

var express = require('express');
var routes = require('./routes'); // Para las funciones con la DB
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var errorhandler = require('errorhandler');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var multer = require('multer');
//var favicon = require('static-favicon');

var app = express();

// all environments
app.set('port', process.env.PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(methodOverride());
app.use(cookieParser());

var passport = require('passport');
var session = require('express-session');

app.use(session({
  secret: 'mySecretKey',
  resave: false,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(app.router);

app.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

// Ruta para autenticarse con Facebook (enlace de login)
app.get('./auth/facebook', passport.authenticate('facebook'));

app.get('./auth/facebook/callback', passport.authenticate('facebook',
  { successRedirect: '/', failureRedirect: '/login' }
));

var flash = require('connect-flash');
app.use(flash());

// initialize passport

var initPassport = require('./passport/init');
initPassport(passport);

app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(errorhandler());
}

var isAuthenticated = function (req, res, next) {

	if (req.isAuthenticated()) {
    return next();
  }
	// if the user is not authenticated then redirect him to the login page
  switch (req.originalUrl) {
    case '/principalJefe':
      redirect = '/jefeDP'
      break;
    case '/principalGradu':
      redirect = '/graduandos'
      break;
    default:
      redirect = '/'
      break;
  }
	res.redirect(redirect);
}

app.get('/login', function(req, res) {
    // Display the Login page with any flash message, if any
  res.render('login', { message: req.flash('message') });
});

/* Handle Login POST */
app.post('/login', loginPost);

function loginPost(req, res, next) {
  passport.authenticate('login', function(err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      var hook = req.headers.referer.includes('jefeDP')
      if(hook) {
        return res.redirect('/jefeDP');
      }
      var hook1 = req.headers.referer.includes('graduandos')
      if(hook1) {
        return res.redirect('/graduandos');
      }
      return res.redirect('/login')
    }
    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }
      switch (user.roll) {
        case 'user':
          redirect = '/principalGradu'
          break;
        case 'admin':
          redirect = '/principalJefe'
          break;
      }
      return res.redirect(redirect);
    });

  })(req, res, next);
}

/* GET Registration Page */
app.get('/signup', isAuthenticated, function(req, res){
  if(req.user.roll === 'office') {
    res.render('registro');
  } else {
    res.redirect('/login');
  }
});

var controllers = require('./passport/signup');
/* Handle Registration POST */
app.post('/registro', controllers.registerUsers);

/* GET Registration Page */
app.get('/signupAdmin', isAuthenticated, function(req, res){
  if(req.user.roll === 'admin') {
    res.render('registerAdmin',{message: req.flash('message')});
  } else {
    res.redirect('/jefeDP');
  }
});

app.post('/signupAdmin', controllers.registerAdmins);

app.get('/signout', function(req, res) {
  req.logout();
  res.redirect('/');
});

app.get('/', routes.getAll);
app.post('/mm', routes.postnew);
app.post('/editPassword', controllers.editPassword);
app.post('/editPasswordAdmin', controllers.editPasswordAdmin);
app.get('/api/entries/:id', routes.getById_complex);
app.get('/api/:id', routes.getById);
app.put('/api/entries/:id', routes.putById);
app.delete('/api/entries/:id', routes.deleteById);
app.get('/api/del/', routes.deleteById);
app.get('/api/search/a/:name', routes.searchByName);
app.get('/api/search/c/:titulo', routes.searchByTitle);

app.get('/agregarProyecto', isAuthenticated, routes.getTesisPersonal)

app.get('/autorBuscar', function(req, res) {
  res.render('autorBuscar')
})
app.get('/autorBuscarG', function(req, res) {
  res.render('autorBuscarG')
})

app.get('/graduandos', function(req, res) {
  res.render('graduandos', { message: req.flash('message') })
})

app.get('/jefeDP', function(req, res) {
  res.render('jefeDP', { message: req.flash('message') })
})

app.get('/principalGradu', isAuthenticated, routes.getGradu)

app.get('/principalJefe', isAuthenticated, routes.getJefe)

app.get('/principal', function(req, res) {
  res.redirect('/')
})
app.get('/tituloBuscarG', function(req, res) {
  res.render('tituloBuscarG')
})

var mongoose = require( 'mongoose' );
//To use the model I created a variable regModel
var regModel = require('./routes/model_db').Tesis;
var User = require('./routes/model_db').User;
var Ini = require ('/routes/model_db').Ini;

app.listen(app.get('port'), function () {
  console.log('Escuchando por el puerto ' + app.get('port'));
})
